import React, { useEffect, useState } from 'react';

function ConferenceForm() {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maxPresentations, setMaxPresentations] = useState('');
  const [maxAttendees, setMaxAttendees] = useState('');
  const [location, setLocation] = useState('');


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);


    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaxPresentations('');
      setMaxAttendees('');
      setLocation('');
    }
  }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    };

    const handleChangeName = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleChangeStarts = (event) => {
        const value = event.target.value;
        setStarts(value);
    }
    const handleChangeEnds = (event) => {
        const value = event.target.value;
        setEnds(value);
    }
    const handleChangeDescription = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handleChangeMaxPresentations = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }
    const handleChangeMaxAttendees = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }
    const handleChangeLocation = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                <input
                    value={name}
                    onChange={handleChangeName}
                    placeholder="Name"
                    required
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"/>
                <label htmlFor="name">Name</label>
                </div>
                <div className="form mb-3">
                <label htmlFor="starts">Start Date</label>
                <input
                    value={starts}
                    onChange={handleChangeStarts}
                    placeholder="Start Date"
                    required
                    type="date"
                    name="starts"
                    id="starts"
                    className="form-control"/>
                </div>
                <div className="form mb-3">
                <label htmlFor="ends">End Date</label>
                <input
                    value={ends}
                    onChange={handleChangeEnds}
                    placeholder="End Date"
                    required
                    type="date"
                    name="ends"
                    id="ends"
                    className="form-control"/>
                </div>
                <div className="form mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea
                    value={description}
                    onChange={handleChangeDescription}
                    className="form-control"
                    id="description"
                    rows="5"
                    placeholder="Write more info"
                    name="description">
                </textarea>
                </div>
                <div className="form-floating mb-3">
                <input
                    value={maxPresentations}
                    onChange={handleChangeMaxPresentations}
                    placeholder="Max Presentations"
                    required
                    type="text"
                    name="max_presentations"
                    id="max_presentations"
                    className="form-control"/>
                <label htmlFor="max_presentations">Max Presentations</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    value={maxAttendees}
                    onChange={handleChangeMaxAttendees}
                    placeholder="Max Attendees"
                    required
                    type="text"
                    name="max_attendees"
                    id="max_attendees"
                    className="form-control"/>
                <label htmlFor="max_attendees">Max Attendees</label>
                </div>
                <div className="mb-3">
                <select
                    value={location}
                    onChange={handleChangeLocation}
                    required
                    name="location"
                    id="location"
                    className="form-select">
                    <option value="">Location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                        );
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default ConferenceForm;
