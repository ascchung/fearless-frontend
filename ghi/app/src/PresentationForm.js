import React, { useState, useEffect } from 'react';

function PresentationForm() {
  const [conferences, setConferences] = useState([]);
  const [ConferenceId, setSelectedConference] = useState('');
  const [presenterName, setPresenterName] = useState('');
  const [presenterEmail, setPresenterEmail] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');

  useEffect(() => {
    async function fetchConferences() {
      try {
        const response = await fetch('http://localhost:8000/api/conferences/');
        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
        }
      } catch (error) {
        console.error('Error fetching conferences:', error);
      }
    }

    fetchConferences();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      presenter_name: presenterName,
      presenter_email: presenterEmail,
      company_name: companyName,
      title,
      synopsis,
      conference: ConferenceId,
    };

    const response = await fetch(`http://localhost:8000/api/conferences/${ConferenceId}/presentations/`, {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      // Reset form fields
      setPresenterName('');
      setPresenterEmail('');
      setCompanyName('');
      setTitle('');
      setSynopsis('');
      setSelectedConference('');
    }
  };

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input
                  onChange={(event) => setPresenterName(event.target.value)}
                  value={presenterName}
                  placeholder="Presenter name"
                  required
                  type="text"
                  id="presenter_name"
                  className="form-control"
                />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={(event) => setPresenterEmail(event.target.value)}
                  value={presenterEmail}
                  placeholder="Presenter email"
                  required
                  type="email"
                  id="presenter_email"
                  className="form-control"
                />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    placeholder="Company name"
                    type="text"
                    name="company_name"
                    id="company_name"
                    className="form-control"
                />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                    placeholder="Title"
                    required type="text"
                    name="title"
                    id="title"
                    className="form-control"
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea
                  onChange={(event) => setSynopsis(event.target.value)}
                  value={synopsis}
                  id="synopsis"
                  className="form-control"
                  rows="3"
                ></textarea>
              </div>
              <div className="mb-3">
                <select
                  onChange={(event) => setSelectedConference(event.target.value)}
                  value={ConferenceId}
                  required
                  className="form-select"
                  id="conference"
                >
                  <option value="">Choose a conference</option>
                  {conferences.map((conference) => (
                    <option key={conference.id} value={conference.id}>
                      {conference.name}
                    </option>
                  ))}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
